<?php

namespace src;

class Calculate
{
    const SEARCH_ELEMENTS = ['(', ')', '[', ']', '{', '}'];
    const OPEN_CLOSE_BRACERS = ['{}', '[]', '()'];

    /**
     * function return bool info about balanced bracers (, ), [, ], {, }
     * @param string $string
     * @return bool
     */
    public function isBraced(string $string): bool
    {
        $bracers = $this->getBracers($string);

        return $this->calculateOpenCloseBracers($bracers);
    }

    /**
     * function return string with result messages
     * @param string $string
     * @param bool $correctlyBraced
     * @return string
     */
    public function printBracerResult(string $string, bool $correctlyBraced): string
    {
        return !$correctlyBraced ? $string . ' - Has an Error<br>' : $string . ' - Has right bracers<br>';
    }

    /**
     * Function returns the array where each value is multiplied by $multiply value
     * @param array $arr
     * @param int $multiply
     * @return array
     */
    public function multiplyArray(array &$arr, int $multiply): array
    {
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                $arr[$key] = self::multiplyArray($val, $multiply);
            } else {
                $arr[$key] *= $multiply;
            }

        }

        return $arr;
    }

    /**
     * leave only bracers in string
     * @param $string
     * @return string
     */
    private function getBracers($string): string
    {
        $bracers = '';
        for ($i = 0; $i < strlen($string); $i += 1) {
            if (in_array($string[$i], self::SEARCH_ELEMENTS)) {
                $bracers .= $string[$i];
            }
        }

        return $bracers;
    }

    /**
     * check if bracers are balanced
     * @param string $bracers
     * @return bool
     */
    private function calculateOpenCloseBracers(string $bracers): bool
    {
        $clearedBracer = str_replace(self::OPEN_CLOSE_BRACERS, [''], $bracers);

        if (strlen($bracers) != strlen($clearedBracer)) {
            if (empty($clearedBracer)) {
                return true;
            } else {
                return self::calculateOpenCloseBracers($clearedBracer);
            }
        } else {
            return false;
        }
    }
}
