# Test Project

## How To Start Project

Download and install Docker from https://www.docker.com/ 
In your IDE: 
##### `git clone git@gitlab.com:pavlysuk/test-project.git`
##### `cd test-project`
##### `docker-compose up -d`
After that you will be able to see project on http://localhost:8086/


##About Project

in project root you can find `index.php` where you can find initialization of calculation class `$calculation = new Calculate();`
This class has 2 methods `isParenthetic` and `multiplyArray`
###### `isBraced` function that accepts a string as input and returns a boolean as output. The function should determine if all parenthetical characters in the string—(, ), [, ], {, }—are balanced, that is, for each opening parenthesis, there is a corresponding closing parenthesis of the same form, and in the reversed sequence as they were opened. Parentheses may be nested.
###### `multiplyArray` function that accepts two arguments, an array of numbers and a single integer, and returns an array similar to the first argument, but with all the values multiplied by the integer
