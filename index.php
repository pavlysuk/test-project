<?php
include "src/Calculate.php";

use src\Calculate;

$calculation = new Calculate();

$testString = 'mary (had a [little] lamb)';
$isPar = $calculation->isBraced($testString);
echo $calculation->printBracerResult($testString, $isPar);


$testString = 'mary (had a [little) lamb]';
$isPar = $calculation->isBraced($testString);
echo $calculation->printBracerResult($testString, $isPar);


$enterArray = [1, 2, [10, [100, 200], 20, 30], 3, [40, 50]];
$exitArray = $enterArray;
$calculation->multiplyArray($exitArray, 2);
echo'<pre>';print_r($enterArray);echo'</pre>';
echo'<pre>';print_r($exitArray);echo'</pre>';exit;
